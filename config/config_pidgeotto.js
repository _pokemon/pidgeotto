var config_pidgeotto = {
    api: {
        connect: {
            version: 'v1',
            resource: 'layout',
            endpoint: 'get_layout',
            other_path: '',
            method: 'GET'
        }
    },
    custom: {
    	menu: {
    		path: '../../views/layout/menu_custom.ejs'
    	},
        menu_horizontal:{
            path: '../../views/layout_dash/menu_dashboard.ejs'
        }
    }
}

module.exports = function() {
    return config_pidgeotto;
}