var layout = require('./layout');
var debug = require('debug')('Ditto:Pidgeotto:index');

module.exports = {
    layout_configure: function(req, res, next) {
    	debug("Fly!");
        layout.fly(req, res, next);
    },
    exception_fly: function(path){
    	debug("Exception Fly!");
    	layout.exception_fly(path);
    }
};