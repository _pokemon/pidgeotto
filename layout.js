var request = require('request');
var redis = require('redis');
var hitmonchan = require('hitmonchan');
var sanitizer = require('sanitizer');
var debug = require('debug')('Ditto:Pidgeotto:layout');
var config_app = require('../../config/config_app')();
var config_pidgeotto = require('./config/config_pidgeotto')();
var config_redis = require('../../config/config_redis')();
var error_app = require('./error.js');
var fs = require('fs');
var path = require('path');
var exports = module.exports = {};
var client = redis.createClient(config_redis.session.port, config_redis.session.host, config_redis.session.options);

client.on("error", function(err) {
    debug("[Connect][Pidgeotto][Redis]" + err);
});

exports.exception_fly = function(path){
    var response = true;
    var exception_layout = config_app.exception_layout;

    Object.keys(exception_layout).forEach(function(index) {
        obj = exception_layout[index];
        if(obj.path == path || (obj.path + '/') == path){
            response = false;
        }
    });
    return response;
}

exports.fly = function(req, res, next) {
    var user = req.app.get('user');
    var app = req.app.get('app');

    try {
        var version = config_pidgeotto.api.connect.version;
        var resource = config_pidgeotto.api.connect.resource;
        var endpoint = config_pidgeotto.api.connect.endpoint;
        var other_path = config_pidgeotto.api.connect.other_path;
        var method = config_pidgeotto.api.connect.method;
        var cookies = {
            active_user_id: sanitizer.escape(req.cookies.charmander),
            user_token: sanitizer.escape(req.cookies.squirtle),
            user_authentication: sanitizer.escape(req.cookies.bulbasaur)
        };
        var qry = {
            client_token: app.data.client_token,
            hash_authentication: app.data.hash_authentication,
            client_token: app.data.client_token,
            hash_authentication: app.data.hash_authentication,
            active_user_id: user.data.id,
            user_token: user.data.token,
            user_hash_authentication: user.data.hash,
            base_path: config_app.app.base_path,
            img_path: config_app.resource.img_path
        };

        key_layout = cookies.active_user_id + ':' + cookies.user_token + ':' + config_app.app.id;
        var exception_layout = config_app.exception_layout;

        Object.keys(exception_layout).forEach(function(index) {
            obj = exception_layout[index];
            if(obj.path == path || (obj.path + '/') == path){
                qry["template"] = obj["menu"];
                key_layout += ":" + obj["menu"];
            }
        });

        var info = {
            query: qry,
            data: null,
            file: null
        };
        debug('prepare_punch');

        var options = hitmonchan.prepare_punch(version, resource,other_path, endpoint, method, info);
        debug('get of redis');

        client.hget(config_redis.session.keys.layout, key_layout, function(err, reply) {
            if (reply != null) {
                debug('Asigner menu from redis');
                req.app.set('layout', reply);
                next();
            } else {
                debug('get route for Pidgeotto!');
                request(options, function(error, response, body) {
                    if (!error) {
                        var resp = JSON.parse(body);
                        var layout = null;

                        try {
                            if (data.app.authorized === false) {
                                client.del(config_redis.session.keys.app, redis.print);
                            }
                        } catch (e) {
                            debug('No find authorized, this is god :D');
                        }

                        if (resp.status) {
                            debug('Asigner menu');
                            fs.readFile(path.join(__dirname, config_pidgeotto.custom.menu.path), 'utf8', function(err, data) {
                                if (err) {
                                    debug('[Pidgeotto][readFile] ->' + err);
                                    layout = resp.data.layout_menu.replace('menu_custom', '');
                                } else {
                                    layout = resp.data.layout_menu.replace('menu_custom', data);
                                }

                                req.app.set('layout', layout);
                                client.hset(config_redis.session.keys.layout, key_layout, layout, redis.print)
                                next();
                            });
                        } else {
                            debug('Problem with layout');
                            debug(resp.message);
                            req.app.set('layout', null);
                            next();
                        }
                    } else {
                        error_app.error(res, 'error', '[Pidgeotto][session] -> ', 'Exception: ' + error, 500, {});
                    }
                });
            }
        });
    } catch (e) {
        error_app.error(res, 'error', '[Pidgeotto][session] -> ', 'Exception: ' + e.message, 500, {});
    }
};